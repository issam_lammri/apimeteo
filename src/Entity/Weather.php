<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weather
 *
 * @ORM\Table(name="weather")
 * @ORM\Entity
 */
class Weather
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateRep", type="date", nullable=false)
     */
    private $daterep;

    /**
     * @var string
     *
     * @ORM\Column(name="countriesAndTerritories", type="string", length=255, nullable=false)
     */
    private $countriesandterritories;

    /**
     * @var int
     *
     * @ORM\Column(name="temperature", type="integer", nullable=false)
     */
    private $temperature;

    /**
     * @var int
     *
     * @ORM\Column(name="humidity", type="integer", nullable=false)
     */
    private $humidity;

    /**
     * @var int
     *
     * @ORM\Column(name="precipitation", type="integer", nullable=false)
     */
    private $precipitation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDaterep(): ?\DateTimeInterface
    {
        return $this->daterep;
    }

    public function setDaterep(\DateTimeInterface $daterep): self
    {
        $this->daterep = $daterep;

        return $this;
    }

    public function getCountriesandterritories(): ?string
    {
        return $this->countriesandterritories;
    }

    public function setCountriesandterritories(string $countriesandterritories): self
    {
        $this->countriesandterritories = $countriesandterritories;

        return $this;
    }

    public function getTemperature(): ?int
    {
        return $this->temperature;
    }

    public function setTemperature(int $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getHumidity(): ?int
    {
        return $this->humidity;
    }

    public function setHumidity(int $humidity): self
    {
        $this->humidity = $humidity;

        return $this;
    }

    public function getPrecipitation(): ?int
    {
        return $this->precipitation;
    }

    public function setPrecipitation(int $precipitation): self
    {
        $this->precipitation = $precipitation;

        return $this;
    }


}
